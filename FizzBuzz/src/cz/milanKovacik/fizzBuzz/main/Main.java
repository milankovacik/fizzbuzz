package cz.milanKovacik.fizzBuzz.main;

import java.util.LinkedHashMap;

import cz.milanKovacik.fizzBuzz.provider.FizzBuzzService;
import cz.milanKovacik.fizzBuzz.userDefine.FizzBuzzUserDefine;
import cz.milanKovacik.fizzBuzz.userDefine.IntegerFizzBuzz;
import cz.milanKovacik.fizzBuzz.userDefine.Settings;

public class Main {

	public static void main(String[] args) {
		System.out.println("----------------Simple----------------");
		simple();
		System.out.println("----------------User define----------------");
		userDefined();
		System.out.println("----------------By provider----------------");
		byProvider();
		
	}

	private static void simple() {
		for(int i = 1; i <= 100; i++) {
			showNumber(i);
		}
	}

	private static void showNumber(int i) {
		if (i % 15 == 0)
			System.out.println("FizzBuzz");
		else if (i % 5 == 0)
			System.out.println("Buzz");
		else if (i % 3 == 0)
			System.out.println("Fizz");
		else
			System.out.println("" + i);
	}

	private static void userDefined() {
		LinkedHashMap<Integer, String> confMap = new LinkedHashMap<>();
		confMap.put(15, "FizzBuzz");
		confMap.put(5, "Buzz");
		confMap.put(3, "Fizz");
		Settings<Integer> settings = new Settings<Integer>(1, 100, 1, confMap);
		FizzBuzzUserDefine fizzBuzz = new IntegerFizzBuzz();
		fizzBuzz.run(settings);
	}

	
	private static void byProvider() {
		FizzBuzzService service = new FizzBuzzService();
		service.run();
	}
}
