package cz.milanKovacik.fizzBuzz.provider;

public class BuzzProvider implements IFizzBuzzProvider<Integer>{

	@Override
	public boolean support(Integer number) {
		return number % 5 == 0;
	}

	@Override
	public void print(Integer number) {
		System.out.println("Buzz");
	}
	
	@Override
	public int getPriority() {
		return 2;
	}

}
