package cz.milanKovacik.fizzBuzz.provider;

public class DefaultFizzBuzzProvider implements IFizzBuzzProvider<Integer>{

	@Override
	public boolean support(Integer number) {
		return true;
	}

	@Override
	public void print(Integer number) {
		System.out.println("" + number);
	}
	
	@Override
	public int getPriority() {
		return 4;
	}

}
