package cz.milanKovacik.fizzBuzz.provider;

public class FizzBuzzProvider implements IFizzBuzzProvider<Integer>{

	@Override
	public boolean support(Integer number) {
		return number % 15 == 0;
	}

	@Override
	public void print(Integer number) {
		System.out.println("FizzBuzz");
	}

	@Override
	public int getPriority() {
		return 1;
	}
}
