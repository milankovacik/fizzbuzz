package cz.milanKovacik.fizzBuzz.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//ins Spring @Service, all providers as @Component
public class FizzBuzzService {

	/*in spring autowired 
	* private final List<IFizzBuzzProvider<Integer>> providerList;
	* 
	* @Autowired
	* public FizzBuzzService(List<IFizzBuzzProvider<Integer>> providerList) {
	* 	this.providerList = providerList.sorted((p1, p2) -> p1.getPriority() > p2.getPriority() ? 1 : 0)
				.collect(Collectors.toList());
	* }
	*/
	
	private static final List<IFizzBuzzProvider<Integer>> PROVIDER_LIST;

	static {
		List<IFizzBuzzProvider<Integer>> providerList = new ArrayList<>();
		providerList.add(new DefaultFizzBuzzProvider());
		providerList.add(new FizzBuzzProvider());
		providerList.add(new BuzzProvider());
		providerList.add(new FizzProvider());

		providerList = providerList.stream().sorted((p1, p2) -> p1.getPriority() > p2.getPriority() ? 0 : -1)
				.collect(Collectors.toList());
		PROVIDER_LIST = providerList;
	}

	public void run() {
		for (int i = 1; i <= 100; i++) {
			getProvider(i).print(i);;
		}
	}

	private IFizzBuzzProvider<Integer> getProvider(int i) {
		for (IFizzBuzzProvider<Integer> provider : PROVIDER_LIST) {
			if (provider.support(i))
				return provider;
		}
		throw new IllegalArgumentException("For number: " + i + " doesn't exist provider");
	}
}
