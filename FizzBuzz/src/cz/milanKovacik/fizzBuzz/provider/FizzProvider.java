package cz.milanKovacik.fizzBuzz.provider;

public class FizzProvider implements IFizzBuzzProvider<Integer>{

	@Override
	public boolean support(Integer number) {
		return number % 3 == 0;
	}

	@Override
	public void print(Integer number) {
		System.out.println("Fizz");
	}

	@Override
	public int getPriority() {
		return 3;
	}

}
