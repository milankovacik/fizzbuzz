package cz.milanKovacik.fizzBuzz.provider;

public interface IFizzBuzzProvider<T extends Number> {
	
	boolean support(T number);
	
	void print(T number);
	
	int getPriority();

}
