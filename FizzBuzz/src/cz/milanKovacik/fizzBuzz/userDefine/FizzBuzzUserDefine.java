package cz.milanKovacik.fizzBuzz.userDefine;

import java.util.LinkedHashMap;

/**
 * User define Fizz Buzz
 * 
 * @author milan
 */
public interface FizzBuzzUserDefine<T extends Number> {

	void showNumber(T i, LinkedHashMap<T, String> confMap);
	
	T add(T i, T step);
	
	default void run(Settings<T> settings) {
		for (T i = settings.getBegin(); i.doubleValue() < settings.getEnd().doubleValue(); i = add(i, settings.getStep())) {
			showNumber(i, settings.getConfMap());
		}
	}
}
