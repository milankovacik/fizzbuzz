package cz.milanKovacik.fizzBuzz.userDefine;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class IntegerFizzBuzz implements FizzBuzzUserDefine<Integer> {

	@Override
	public void showNumber(Integer i, LinkedHashMap<Integer, String> confMap) {
		for (Entry<Integer, String> cnf : confMap.entrySet()) {
			if(i % cnf.getKey() == 0) {
				System.out.println(cnf.getValue());
				return;
			}
		}
		System.out.println(i.toString());
	}

	@Override
	public Integer add(Integer i, Integer step) {
		return i + step;
	}
}
