package cz.milanKovacik.fizzBuzz.userDefine;

import java.util.LinkedHashMap;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Settings<T extends Number> {

	private final T begin;
	private final T end;
	private final T step;
	LinkedHashMap<T, String> confMap;


}
